import sys
from sqlalchemy import create_engine
from search import search_by_code
from sqlalchemy.orm import sessionmaker
from atemplate import Airport
from vincenty import vincenty

engine = create_engine('mysql://root:pass@127.0.0.1:3316/mydb', echo=True)

if __name__ == "__main__":
    Session_distance = sessionmaker(bind=engine)
    s = Session_distance()
    CODE_1 = sys.argv[1]
    CODE_2 = sys.argv[2]
    code_1 = search_by_code(CODE_1, Airport, s)
    code_2 = search_by_code(CODE_2, Airport, s)
    S = vincenty([float(code_1['latitude']), float(code_1['longitude'])], [float(code_2['latitude']),
                                                                           float(code_2['longitude'])])
    print("distance: ", S)
