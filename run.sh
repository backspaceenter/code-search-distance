#!/bin/bash
sudo apt-get install mysql-client libmysqlclient-dev python3-dev
pyenv install $(cat .python-version)
pip install -r requirements.txt

docker run --name=mysql -e MYSQL_ROOT_HOST='%' -e MYSQL_ROOT_USERNAME=root -e MYSQL_ROOT_PASSWORD=pass -v "$(pwd)/data:/var/lib/mysql" -p 3316:3306 --rm -d mysql:5.7 mysqld &

while ! mysqladmin ping -h"127.0.0.1" -P 3316 --silent; do
  echo 'ping'
  sleep 1
done


echo 'ready'
#sleep 9999

python ./db_init.py
python ./search.py UUBW
python ./distance.py UUEE UUDD
docker stop mysql
